import {Component} from '@angular/core';
import {CarModelsService} from "@services/cars/car-models.service";
import {ICarBrand} from "@models/car-brand";
import {Subscription} from "rxjs";
import {ICarModel} from "@models/car-models";
import {Router} from "@angular/router";
import {CarBrandsService} from "@services/cars/car-brands.service";
import {PrimeNGConfig} from "primeng/api";

@Component({
  selector: 'app-car-models-list',
  templateUrl: './car-models-list.component.html',
  styleUrls: ['./car-models-list.component.scss']
})
export class CarModelsListComponent {

  cols = [
    // {field: '_id', header: 'Brand ID'},
    {field: 'brand', header: 'Бренд', element: true},
    {field: 'name', header: 'Название модели'},
  ];


  constructor(
    private carModelsService: CarModelsService,
    private carBrandsService: CarBrandsService,
    private router: Router,
    private config: PrimeNGConfig
  ) {
  }

  carModels: ICarModel[] = [];
  carBrands: ICarBrand[] = [];
  carBrandNames: string[] = [];

  private carBrandUnsubscribes: Subscription = this.carBrandsService
    .carBrandsUpdateSubject$
    .subscribe((data) => {
      this.setBrands(data);
    });

  private carModelUnsubscribes: Subscription = this.carModelsService
    .carModelsUpdateSubject$
    .subscribe((data) => {
      this.setModels(data);
    });

  setModels(models: ICarModel[]) {
    this.carModels = models;
  }

  setBrands(brands: ICarBrand[]) {
    this.carBrands = brands;
    let brandNames: string[] = [];
    brands.map((brand: ICarBrand) => {
      brandNames.push(brand.name)
    });
    this.carBrandNames = brandNames;
  }


  ngOnInit(): void {
    this.config.setTranslation({
      apply: 'Применить',
      clear: 'Очистить',
      //translations
    });
    this.carModelsService.getModels()
      .subscribe({
        next: (data) => {
          this.carModelsService.updateCarModelsSubs(data);
        }
      })

    this.carBrandsService
      .getBrands()
      .subscribe((res: ICarBrand[]) => {
        this.carBrandsService.updateCarBrandsSubs(res);
      });
  }

  ngOnDestroy(): void {
    this.carModelUnsubscribes.unsubscribe();
    this.carBrandUnsubscribes.unsubscribe();
  }

  createNewModel() {
    this.router.navigate(['admin/car-models/new']);
  }
}
