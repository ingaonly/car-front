import {LOCALE_ID, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {CarModelsRoutingModule} from './car-models-routing.module';
import {CarModelsComponent} from './car-models/car-models.component';
import {CarModelsListComponent} from './car-models-list/car-models-list.component';
import {CarModelsNewComponent} from './car-models-new/car-models-new.component';
import {ButtonModule} from "primeng/button";
import {SharedModule} from "primeng/api";
import {TableModule} from "primeng/table";
import {MultiSelectModule} from "primeng/multiselect";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {InputTextModule} from "primeng/inputtext";
import {RippleModule} from "primeng/ripple";
import {DropdownModule} from "primeng/dropdown";


@NgModule({
  declarations: [
    CarModelsComponent,
    CarModelsListComponent,
    CarModelsNewComponent
  ],
  imports: [
    CommonModule,
    CarModelsRoutingModule,
    ButtonModule,
    SharedModule,
    TableModule,
    MultiSelectModule,
    FormsModule,
    ReactiveFormsModule,
    InputTextModule,
    RippleModule,
    DropdownModule
  ],
  providers: [
    {provide: LOCALE_ID, useValue: "ru-RU"},
  ]
})
export class CarModelsModule {
}
