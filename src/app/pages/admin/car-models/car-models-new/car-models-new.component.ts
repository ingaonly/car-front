import {Component} from '@angular/core';
import {CarBrandsService} from "@services/cars/car-brands.service";
import {MessageService} from "primeng/api";
import {Router} from "@angular/router";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {ICarBrand} from "@models/car-brand";
import {Subscription} from "rxjs";
import {CarModelsService} from "@services/cars/car-models.service";
import {HttpErrorResponse} from "@angular/common/http";
import {ServerError} from "@models/errors";

@Component({
  selector: 'app-car-models-new',
  templateUrl: './car-models-new.component.html',
  styleUrls: ['./car-models-new.component.scss']
})
export class CarModelsNewComponent {

  constructor(
    private carBrandsService: CarBrandsService,
    private carModelsService: CarModelsService,
    private messageService: MessageService,
    private router: Router
  ) {
  }

  ngOnInit(): void {
    this.carBrandsService
      .getBrands()
      .subscribe((res: ICarBrand[]) => {
        this.carBrandsService.updateCarBrandsSubs(res);
      });
  }

  carBrands: ICarBrand[] = [];

  private carBrandUnsubscribes: Subscription = this.carBrandsService
    .carBrandsUpdateSubject$
    .subscribe((data) => {
      this.setBrands(data);
    });

  setBrands(brands: ICarBrand[]) {
    this.carBrands = brands;
  }

  newModelForm: FormGroup = new FormGroup(
    {
      name: new FormControl('', [
        Validators.required,
        Validators.minLength(1),
      ]),
      brandID: new FormControl(1, [Validators.required,])
    }
  );
  editClientObj: any;

  click() {
    console.log(this.newModelForm.value.brandID, this.newModelForm.value.name);

    this.carModelsService.createModel(this.newModelForm.value.brandID, this.newModelForm.value.name)
      .subscribe({
          next: (res) => {
            console.log('res', res);
            this.router.navigate(['/admin/car-models'])
              .then(() => {
                this.messageService.add({
                  severity: 'success',
                  summary: 'Модель добавлена',
                  detail: res.brand.name + ' ' + res.name
                });
              });
          },
          error: (error: HttpErrorResponse) => {
            console.log('Reg log:', error);
            const serverError = <ServerError>error.error;
            this.messageService.add({
              severity: 'error',
              summary: 'Ошибка создания модели',
              detail: serverError.errorText ?? 'Ошибка'
            });
          }
        }
      );
  }

  ngOnDestroy(): void {
    this.carBrandUnsubscribes.unsubscribe();
  }
}
