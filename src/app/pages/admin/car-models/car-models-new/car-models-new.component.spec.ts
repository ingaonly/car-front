import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CarModelsNewComponent } from './car-models-new.component';

describe('CarModelsNewComponent', () => {
  let component: CarModelsNewComponent;
  let fixture: ComponentFixture<CarModelsNewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CarModelsNewComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CarModelsNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
