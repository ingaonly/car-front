import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CarModelsComponent} from "@app/pages/admin/car-models/car-models/car-models.component";
import {CarModelsListComponent} from "@app/pages/admin/car-models/car-models-list/car-models-list.component";
import {CarModelsNewComponent} from "@app/pages/admin/car-models/car-models-new/car-models-new.component";

const routes: Routes = [
  {
    path: '',
    component: CarModelsComponent,
    children: [
      {
        path: '',
        component: CarModelsListComponent
      },
      {
        path: 'new',
        component: CarModelsNewComponent
      },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CarModelsRoutingModule {
}
