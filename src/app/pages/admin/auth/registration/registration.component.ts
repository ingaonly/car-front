import {Component} from '@angular/core';
import {AuthService} from "@services/auth/auth.service";
import {MessageService} from "primeng/api";
import {Router} from "@angular/router";
import {
  AbstractControl,
  FormControl,
  FormGroup,
  ValidationErrors,
  Validators
} from "@angular/forms";
import {HttpErrorResponse} from "@angular/common/http";
import {ServerError} from "@models/errors";

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent {
  registrationForm: FormGroup = new FormGroup(
    {
      email: new FormControl('', [
        Validators.required,
        Validators.minLength(3),
      ]),
      name: new FormControl('', [
        Validators.required,
        Validators.minLength(3),
      ]),
      phone: new FormControl('', [
        Validators.required,
        Validators.minLength(6),
      ]),
      password: new FormControl('', [
        Validators.required,
        Validators.minLength(3),
      ]),
      passwordConfirm: new FormControl('', [
        Validators.required,
        Validators.minLength(3),
      ])
    }
  );


  constructor(private authService: AuthService,
              private messageService: MessageService,
              private router: Router
  ) {
    this.registrationForm.addValidators(this.matchPassword);
  }

  ngOnInit(): void {

  }

  click() {
    console.log(this.registrationForm.value)


    this.authService.registerUser(
      this.registrationForm.value.email,
      this.registrationForm.value.password,
      this.registrationForm.value.name,
      this.registrationForm.value.phone)
      .subscribe({
          next: () => {
            this.router.navigate(['admin/login'])
              .then(() => {
                this.messageService.add({
                  severity: 'success',
                  summary: 'Вы успешно зарегистрировались!',
                  detail: 'Теперь вы можете войти в систему'
                });
              })
          },
          error: (error: HttpErrorResponse) => {
            console.log('Reg log:', error);
            const serverError = <ServerError>error.error;
            this.messageService.add({
              severity: 'error',
              summary: 'Ошибка регистрации',
              detail: serverError.errorText ?? 'Ошибка'
            });
          }
        }
      );
  }


  matchPassword(control: AbstractControl): ValidationErrors | null {
    if (control === null || control === undefined) {
      return null;
    }
    const password = control.get("password")?.value;
    const confirm = control.get("passwordConfirm")?.value;
    if (password != confirm) {
      return {'noMatch': true}
    }
    return null
  }


}
