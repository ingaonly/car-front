import {Component} from '@angular/core';
import {AuthService} from "@services/auth/auth.service";
import {HttpErrorResponse} from "@angular/common/http";
import {ServerError} from "@models/errors";
import {MessageService} from "primeng/api";
import {Router} from "@angular/router";


@Component({
  selector: 'app-authorization',
  templateUrl: './authorization.component.html',
  styleUrls: ['./authorization.component.scss']
})
export class AuthorizationComponent {
  public email: string = '';
  public password: string = '';

  constructor(private authService: AuthService,
              private messageService: MessageService,
              private router: Router
  ) {
  }

  ngOnInit(): void {

  }

  click() {
    this.authService.authUser(this.email, this.password)
      .subscribe({
          next: (data) => {
            console.log('Auth log:', data);
            this.authService.setSession(data);
            this.messageService.add({
              severity: 'info',
              summary: 'Вы успешно авторизовались',
            });
            this.router.navigate(['admin/']);
          },
          error: (error: HttpErrorResponse) => {
            console.log('Auth log:', error);
            const serverError = <ServerError>error.error;
            this.messageService.add({
              severity: 'error',
              summary: 'Ошибка авторизации',
              detail: serverError.errorText ?? 'Ошибка'
            });
          }
        }
      );
  }
}
