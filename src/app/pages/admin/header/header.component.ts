import {Component} from '@angular/core';
import {MenuItem} from "primeng/api";
import {Router} from "@angular/router";
import {registerLocaleData} from "@angular/common";
import localeRu from '@angular/common/locales/ru';
import {AuthService} from "@services/auth/auth.service";
import {IUser} from "@models/users";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {

  items: MenuItem[] = [];
  user?: IUser= undefined;
  userButtonText = 'Пользователь';

  constructor(
    private auth: AuthService,
    private router: Router
  ) {
  }

  ngOnInit(): void {

    this.auth.getCurrentUser().subscribe(
      {
        next: (user) => {
          this.user=user;
          if (user && user.name) {
            this.userButtonText = user.name;
          }
          this.initMenu();
        },
        error: err => {
          console.log(err);
        }
      }
    )

    registerLocaleData(localeRu, 'ru-RU');
  }

  private initMenu() {
    this.items = [
      {
        label: 'Панель управления',
        routerLink: ['/admin'],
      },
      {
        label: 'Машины',
        routerLink: ['cars'],
      },
      {
        label: 'Бренды',
        routerLink: ['car-brands'],
      },
      {
        label: 'Модели',
        routerLink: ['car-models'],
      },      {
        label: 'Заявки',
        routerLink: ['orders'],
      },
      {
        label: 'Вернуться на сайт',
        routerLink: ['/'],
      },
      {
        label: this.userButtonText,
        icon: 'pi pi-fw pi-user',
        items: [
          {
            label: 'Выйти',

            command: () => {
              this.auth.removeToken();
              this.router.navigate(['/admin/login']);

            }
          }
        ]

      },
    ];
  }
}
