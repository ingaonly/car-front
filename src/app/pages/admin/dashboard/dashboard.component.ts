import {Component} from '@angular/core';
import {CarModelsService} from "@services/cars/car-models.service";
import {CarBrandsService} from "@services/cars/car-brands.service";
import {CarService} from "@services/cars/car.service";
import {ICarModel} from "@models/car-models";
import {ICarBrand} from "@models/car-brand";
import {ICar, ICarFlatBrand} from "@models/car";
import {Subscription} from "rxjs";
import {OrderService} from "@services/order/order.service";
import {IOrder} from "@models/order";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent {
  constructor(
    private carModelsService: CarModelsService,
    private carBrandsService: CarBrandsService,
    private carService: CarService,
    private ordersService: OrderService,
  ) {
  }

  carModels: ICarModel[] = [];
  carBrands: ICarBrand[] = [];
  cars: ICarFlatBrand[] = [];
  orders: IOrder[] =[];

  ngOnInit(): void {
    this.carModelsService.getModels()
      .subscribe({
        next: (data) => {
          this.carModelsService.updateCarModelsSubs(data);
        }
      })

    this.carBrandsService
      .getBrands()
      .subscribe((res: ICarBrand[]) => {
        this.carBrandsService.updateCarBrandsSubs(res);
      });

    this.carService
      .getCars()
      .subscribe((res: ICar[]) => {
        this.carService.updateCarModelsSubs(res);
      });

    this.ordersService
      .getOrders()
      .subscribe((res:IOrder[])=>{
        this.ordersService.updateOrdersSubs(res);
      })
  }

  ngOnDestroy(): void {
    this.carModelUnsubscribes.unsubscribe();
    this.carBrandUnsubscribes.unsubscribe();
    this.carUnsubscribes.unsubscribe();
    this.orderUnsubscribes.unsubscribe();
  }

  private carBrandUnsubscribes: Subscription = this.carBrandsService
    .carBrandsUpdateSubject$
    .subscribe((data) => {
      this.setBrands(data);
    });

  private carModelUnsubscribes: Subscription = this.carModelsService
    .carModelsUpdateSubject$
    .subscribe((data) => {
      this.setModels(data);
    });

  private carUnsubscribes: Subscription = this.carService
    .carsUpdateSubject$
    .subscribe((data: ICar[]) => {
      this.setCars(data);
    });

  private orderUnsubscribes: Subscription =this.ordersService
    .ordersUpdateSubject$
    .subscribe((data:IOrder[])=>{
      this.setOrders(data);
    })

  private setModels(models: ICarModel[]) {
    this.carModels = models;
  }

  private setBrands(brands: ICarBrand[]) {
    this.carBrands = brands;
  }

  private setOrders(orders: IOrder[]) {
    this.orders = orders;
  }

  private setCars(cars: ICarFlatBrand[]) {
    cars.map((car: ICarFlatBrand) => {
      car.brand = car.model.brand;
    });
    this.cars = cars;
  }
}
