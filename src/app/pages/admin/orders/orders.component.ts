import {Component} from '@angular/core';
import {CarModelsService} from "@services/cars/car-models.service";
import {CarBrandsService} from "@services/cars/car-brands.service";
import {CarService} from "@services/cars/car.service";
import {Router} from "@angular/router";
import {PrimeNGConfig} from "primeng/api";
import {OrderService} from "@services/order/order.service";
import {Subscription} from "rxjs";
import {IOrder, IOrderFlat} from "@models/order";
import {ICarFlatBrand} from "@models/car";

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss']
})
export class OrdersComponent {
  cols = [
    {field: 'manager', header: 'Менеджер', manager: true},
    {field: 'car', header: 'Машина', car: true},
    {field: 'name', header: 'Имя клиента'},
    {field: 'phone', header: 'Телефон'},
    {field: 'date', header: 'Дата создания заявки', date: true},
  ];

  constructor(
    private carModelsService: CarModelsService,
    private carBrandsService: CarBrandsService,
    private carService: CarService,
    private orderService: OrderService,
    private router: Router,
    private config: PrimeNGConfig
  ) {
  }

  orders: IOrderFlat[] = [];


  ngOnInit(): void {
    this.config.setTranslation({
      apply: 'Применить',
      clear: 'Очистить',
      //translations
    });

    this.orderService.getOrders()
      .subscribe({
        next: (data) => {
          this.orderService.updateOrdersSubs(data);
        }
      });

  }

  private ordersUnsubscribes: Subscription = this.orderService
    .ordersUpdateSubject$
    .subscribe((data) => {
      this.setOrders(data);
    });

  ngOnDestroy(): void {
    this.ordersUnsubscribes.unsubscribe();
  }

  setOrders(data: IOrder[]): void {
    this.orders = data.map((order) => {
      let car:ICarFlatBrand = order.car;
      car.brand = car.model.brand;
      return {
        manager: order.car?.manager,
        car: car,
        name: order.name,
        phone: order.phone,
        date: order.date
      }
    });
  }

  chooseCar(car: ICarFlatBrand) {
    const brandName = car.brand?.name ?? 'brand';
    const modelName = car.model?.name ?? 'model';
    // this.router.navigate(['/car', brandName, modelName, car.year, car._id]);


    const url = this.router.serializeUrl(
      this.router.createUrlTree(['/car', brandName, modelName, car.year, car._id])
    );

    window.open(url, '_blank');

  }

}
