import {Component} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {CarBrandsService} from "@services/cars/car-brands.service";
import {MessageService} from "primeng/api";
import {ActivatedRoute, Router} from "@angular/router";
import {ICarBrand} from "@models/car-brand";
import {HttpErrorResponse} from "@angular/common/http";
import {ServerError} from "@models/errors";

@Component({
  selector: 'app-car-brands-edit',
  templateUrl: './car-brands-edit.component.html',
  styleUrls: ['./car-brands-edit.component.scss']
})
export class CarBrandsEditComponent {

  editBrandForm: FormGroup = new FormGroup(
    {
      name: new FormControl('', [
        Validators.required,
        Validators.minLength(1),
      ]),
    }
  );

  carBrandID?: string;
  carBrand?: ICarBrand;

  constructor(
    private carBrandsService: CarBrandsService,
    private messageService: MessageService,
    private router: Router,
    private route: ActivatedRoute,
  ) {
  }

  ngOnInit(): void {
    // parse url params
    const routeIdParam = this.route.snapshot.paramMap.get('id');
    const queryIdParam = this.route.snapshot.queryParamMap.get('id');

    const paramId = routeIdParam || queryIdParam;
    if (paramId) {
      this.carBrandID = paramId;
      this.carBrandsService.getBrandByID(this.carBrandID)
        .subscribe(
          {
            next: (res) => {
              this.carBrand = res;
              this.editBrandForm.controls['name']?.setValue(this.carBrand.name);
            }
          }
        );
    }

  }

  click() {
    if (!this.carBrandID) {
      return; // error
    }
    this.carBrandsService.updateBrandByID(this.carBrandID, this.editBrandForm.value.name)
      .subscribe({
          next: (res) => {
            console.log('res', res);
            this.router.navigate(['/admin/car-brands'])
              .then(() => {
                this.messageService.add({
                  severity: 'success',
                  summary: 'Бренд изменён',
                  detail: res.name
                });
              });
          },
          error: (error: HttpErrorResponse) => {
            console.log('Reg log:', error);
            const serverError = <ServerError>error.error;
            this.messageService.add({
              severity: 'error',
              summary: 'Ошибка изменения бренда',
              detail: serverError.errorText ?? 'Ошибка'
            });
          }
        }
      );
  }
}
