import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CarBrandsComponent} from "@app/pages/admin/car-brands/car-brands.component";
import {CarBrandsListComponent} from "@app/pages/admin/car-brands/car-brands-list/car-brands-list.component";
import {CarBrandsNewComponent} from "@app/pages/admin/car-brands/car-brands-new/car-brands-new.component";
import {CarBrandsEditComponent} from "@app/pages/admin/car-brands/car-brands-edit/car-brands-edit.component";

const routes: Routes = [
  {
    path: '',
    component: CarBrandsComponent,
    children: [
      {
        path: '',
        component: CarBrandsListComponent
      },
      {
        path: 'new',
        component: CarBrandsNewComponent
      },
      {
        path: 'edit/:id',
        component: CarBrandsEditComponent
      },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CarBrandsRoutingModule {
}
