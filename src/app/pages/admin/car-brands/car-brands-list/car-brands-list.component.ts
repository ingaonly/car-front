import {Component} from '@angular/core';
import {CarBrandsService} from "@services/cars/car-brands.service";
import {ICarBrand} from "@models/car-brand";
import {Subscription} from "rxjs";
import {MessageService} from "primeng/api";
import {Router} from "@angular/router";

@Component({
  selector: 'app-car-brands-list',
  templateUrl: './car-brands-list.component.html',
  styleUrls: ['./car-brands-list.component.scss']
})
export class CarBrandsListComponent {

  constructor(
    private carBrandsService: CarBrandsService,
    private messageService: MessageService,
    private router: Router,
  ) {
  }

  cols = [
    // {field: '_id', header: 'Brand ID'},
    {field: 'name', header: 'Brand Name'},
  ];

  carBrands: ICarBrand[] = [];
  private carUnsubscribes: Subscription = this.carBrandsService
    .carBrandsUpdateSubject$
    .subscribe((data) => {
      this.setBrands(data);
    });

  ngOnInit(): void {
    this.carBrandsService
      .getBrands()
      .subscribe((res: ICarBrand[]) => {
        this.carBrandsService.updateCarBrandsSubs(res);
      });
  }

  setBrands(brands: ICarBrand[]) {
    console.log('brands', brands);
    this.carBrands = brands;
  }

  ngOnDestroy(): void {
    this.carUnsubscribes.unsubscribe();
  }

  onRowEditInit(carBrand: ICarBrand) {
    if (!carBrand._id) {
      return this.messageService.add({
        severity: 'error',
        summary: 'Ошибка',
        detail: 'Не удалось получить id бренда'

      });
    }
    console.log('foobar')
    this.router.navigate(['admin/car-brands/edit', carBrand._id]);
  }

  createNewBrand() {
    this.router.navigate(['admin/car-brands/new']);
  }
}
