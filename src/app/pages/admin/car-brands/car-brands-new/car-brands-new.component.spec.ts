import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CarBrandsNewComponent } from './car-brands-new.component';

describe('CarBrandsNewComponent', () => {
  let component: CarBrandsNewComponent;
  let fixture: ComponentFixture<CarBrandsNewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CarBrandsNewComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CarBrandsNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
