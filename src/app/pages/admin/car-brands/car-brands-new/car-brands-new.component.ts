import {Component} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {CarBrandsService} from "@services/cars/car-brands.service";
import {HttpErrorResponse} from "@angular/common/http";
import {ServerError} from "@models/errors";
import {MessageService} from "primeng/api";
import {Router} from "@angular/router";


@Component({
  selector: 'app-car-brands-new',
  templateUrl: './car-brands-new.component.html',
  styleUrls: ['./car-brands-new.component.scss']
})
export class CarBrandsNewComponent {
  newBrandForm: FormGroup = new FormGroup(
    {
      name: new FormControl('', [
        Validators.required,
        Validators.minLength(1),
      ]),
    }
  );

  constructor(
    private carBrandsService: CarBrandsService,
    private messageService: MessageService,
    private router: Router
  ) {
  }

  ngOnInit(): void {

  }

  click() {
    this.carBrandsService.createBrand(this.newBrandForm.value.name)
      .subscribe({
          next: (res) => {
            console.log('res', res);
            this.router.navigate(['/admin/car-brands'])
              .then(() => {
                this.messageService.add({
                  severity: 'success',
                  summary: 'Бренд добавлен',
                  detail: res.name
                });
              });
          },
          error: (error: HttpErrorResponse) => {
            console.log('Reg log:', error);
            const serverError = <ServerError>error.error;
            this.messageService.add({
              severity: 'error',
              summary: 'Ошибка создания бренда',
              detail: serverError.errorText ?? 'Ошибка'
            });
          }
        }
      );

  }

}
