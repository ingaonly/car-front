import {LOCALE_ID, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TableModule} from "primeng/table";

import {CarBrandsRoutingModule} from './car-brands-routing.module';
import {CarBrandsComponent} from './car-brands.component';
import {CarBrandsListComponent} from './car-brands-list/car-brands-list.component';
import {CarBrandsNewComponent} from './car-brands-new/car-brands-new.component';
import {ButtonModule} from "primeng/button";
import {InputTextModule} from "primeng/inputtext";
import {ReactiveFormsModule} from "@angular/forms";
import {RippleModule} from "primeng/ripple";
import { CarBrandsEditComponent } from './car-brands-edit/car-brands-edit.component';


@NgModule({
  declarations: [
    CarBrandsComponent,
    CarBrandsListComponent,
    CarBrandsNewComponent,
    CarBrandsEditComponent,
  ],
  imports: [
    CommonModule,
    TableModule,
    CarBrandsRoutingModule,
    ButtonModule,
    InputTextModule,
    ReactiveFormsModule,
    RippleModule
  ],
  providers: [
    {provide: LOCALE_ID, useValue: "ru-RU"},
  ]
})
export class CarBrandsModule {
}
