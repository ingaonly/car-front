import {LOCALE_ID, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {AdminRoutingModule} from './admin-routing.module';
import {AdminComponent} from './admin.component';
import {HeaderComponent} from './header/header.component';
import {FooterComponent} from './footer/footer.component';
import {MenubarModule} from "primeng/menubar";
import {DashboardComponent} from './dashboard/dashboard.component';
import {MessageService} from "primeng/api";
import {ToastModule} from "primeng/toast";
import {ButtonModule} from "primeng/button";
import {RippleModule} from "primeng/ripple";
import {InputTextModule} from "primeng/inputtext";
import { OrdersComponent } from './orders/orders.component';
import {TableModule} from "primeng/table";


@NgModule({
  declarations: [
    AdminComponent,
    HeaderComponent,
    FooterComponent,
    DashboardComponent,
    OrdersComponent
  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    MenubarModule,
    ToastModule,
    ButtonModule,
    RippleModule,
    InputTextModule,
    TableModule
  ],
  providers: [
    {provide: LOCALE_ID, useValue: "ru-RU"},
    MessageService,
    // {provide: HTTP_INTERCEPTORS, useClass: RestInterceptor, multi: true}
  ]
})
export class AdminModule {
}
