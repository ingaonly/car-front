import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CarsComponent} from "@app/pages/admin/cars/cars/cars.component";
import {CarsListComponent} from "@app/pages/admin/cars/cars-list/cars-list.component";
import {CarsNewComponent} from "@app/pages/admin/cars/cars-new/cars-new.component";

const routes: Routes = [
  {
    path: '',
    component: CarsComponent,
    children: [
      {
        path: '',
        component: CarsListComponent
      },
      {
        path: 'new',
        component: CarsNewComponent
      },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CarsRoutingModule {
}
