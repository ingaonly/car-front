import {Component} from '@angular/core';
import {CarModelsService} from "@services/cars/car-models.service";
import {CarBrandsService} from "@services/cars/car-brands.service";
import {Router} from "@angular/router";
import {PrimeNGConfig} from "primeng/api";
import {CarService} from "@services/cars/car.service";
import {ICarModel} from "@models/car-models";
import {ICarBrand} from "@models/car-brand";
import {ICar, ICarFlatBrand} from "@models/car";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-cars-list',
  templateUrl: './cars-list.component.html',
  styleUrls: ['./cars-list.component.scss']
})
export class CarsListComponent {
  cols = [
    {field: 'brand', header: 'Бренд', element: true},
    {field: 'model', header: 'Модель', element: true},
    {field: 'manager', header: 'Менеджер', element: true},
    {field: 'color', header: 'Цвет'},
    {field: 'price', header: 'Цена'},
  ];

  constructor(
    private carModelsService: CarModelsService,
    private carBrandsService: CarBrandsService,
    private carService: CarService,
    private router: Router,
    private config: PrimeNGConfig
  ) {
  }

  carModels: ICarModel[] = [];
  carBrands: ICarBrand[] = [];
  carBrandNames: string[] = [];
  cars: ICarFlatBrand[] = [];

  ngOnInit(): void {
    this.config.setTranslation({
      apply: 'Применить',
      clear: 'Очистить',
      //translations
    });
    this.carModelsService.getModels()
      .subscribe({
        next: (data) => {
          this.carModelsService.updateCarModelsSubs(data);
        }
      })

    this.carBrandsService
      .getBrands()
      .subscribe((res: ICarBrand[]) => {
        this.carBrandsService.updateCarBrandsSubs(res);
      });

    this.carService
      .getCars()
      .subscribe((res: ICar[]) => {
        this.carService.updateCarModelsSubs(res);
      });
  }

  ngOnDestroy(): void {
    this.carModelUnsubscribes.unsubscribe();
    this.carBrandUnsubscribes.unsubscribe();
    this.carUnsubscribes.unsubscribe();
  }

  createNewCar() {
    this.router.navigate(['admin/cars/new']);
  }

  private carBrandUnsubscribes: Subscription = this.carBrandsService
    .carBrandsUpdateSubject$
    .subscribe((data) => {
      this.setBrands(data);
    });

  private carModelUnsubscribes: Subscription = this.carModelsService
    .carModelsUpdateSubject$
    .subscribe((data) => {
      this.setModels(data);
    });

  private carUnsubscribes: Subscription = this.carService
    .carsUpdateSubject$
    .subscribe((data: ICar[]) => {
      this.setCars(data);
    });

  private setModels(models: ICarModel[]) {
    this.carModels = models;
  }

  private setBrands(brands: ICarBrand[]) {
    this.carBrands = brands;
    let brandNames: string[] = [];
    brands.map((brand: ICarBrand) => {
      brandNames.push(brand.name)
    });
    this.carBrandNames = brandNames;
  }

  private setCars(cars: ICarFlatBrand[]) {
    cars.map((car: ICarFlatBrand) => {
      car.brand = car.model.brand;
    });

    this.cars = cars;
  }

  chooseCar(car: ICarFlatBrand) {
    const brandName = car.brand?.name ?? 'brand';
    const modelName = car.model?.name ?? 'model';
    // this.router.navigate(['/car', brandName, modelName, car.year, car._id]);


    const url = this.router.serializeUrl(
      this.router.createUrlTree(['/car', brandName, modelName, car.year, car._id])
    );

    window.open(url, '_blank');

  }

}
