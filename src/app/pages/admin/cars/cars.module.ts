import {LOCALE_ID, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {CarsRoutingModule} from './cars-routing.module';
import {CarsNewComponent} from './cars-new/cars-new.component';
import {CarsComponent} from "@app/pages/admin/cars/cars/cars.component";
import {CarsListComponent} from "@app/pages/admin/cars/cars-list/cars-list.component";
import {TableModule} from "primeng/table";
import {MultiSelectModule} from "primeng/multiselect";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {ButtonModule} from "primeng/button";
import {DropdownModule} from "primeng/dropdown";
import {InputTextModule} from "primeng/inputtext";
import {RippleModule} from "primeng/ripple";
import {InputTextareaModule} from "primeng/inputtextarea";


@NgModule({
  declarations: [
    CarsComponent,
    CarsNewComponent,
    CarsListComponent
  ],
    imports: [
        CommonModule,
        CarsRoutingModule,
        TableModule,
        MultiSelectModule,
        FormsModule,
        ButtonModule,
        ReactiveFormsModule,
        DropdownModule,
        InputTextModule,
        RippleModule,
        InputTextareaModule
    ],
  providers: [
    {provide: LOCALE_ID, useValue: "ru-RU"},
  ]
})
export class CarsModule {
}
