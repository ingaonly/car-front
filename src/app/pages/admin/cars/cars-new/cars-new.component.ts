import {Component} from '@angular/core';
import {CarBrandsService} from "@services/cars/car-brands.service";
import {CarModelsService} from "@services/cars/car-models.service";
import {MessageService} from "primeng/api";
import {Router} from "@angular/router";
import {ICarBrand} from "@models/car-brand";
import {Subscription} from "rxjs";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {HttpErrorResponse} from "@angular/common/http";
import {ServerError} from "@models/errors";
import {ICarModel} from "@models/car-models";
import {CarService} from "@services/cars/car.service";
import {ICarColor} from "@models/car-color";

@Component({
  selector: 'app-cars-new',
  templateUrl: './cars-new.component.html',
  styleUrls: ['./cars-new.component.scss']
})
export class CarsNewComponent {
  constructor(
    private carBrandsService: CarBrandsService,
    private carModelsService: CarModelsService,
    private carService: CarService,
    private messageService: MessageService,
    private router: Router
  ) {
  }

  ngOnInit(): void {
    this.carBrandsService
      .getBrands()
      .subscribe((res: ICarBrand[]) => {
        this.carBrandsService.updateCarBrandsSubs(res);
      });

    this.carModelsService.getModels()
      .subscribe({
        next: (data) => {
          this.carModelsService.updateCarModelsSubs(data);
        }
      });
  }

  carBrands: ICarBrand[] = [];
  carModels: ICarModel[] = [];
  carColors:ICarColor[]=[
    {name:'черный', value:'черный'},
    {name:'синий', value:'синий'},
    {name:'белый', value:'белый'},
    {name:'красный', value:'красный'},
    {name:'серый металлик', value:'серый металлик'}
  ];

  carModelsMap: { [key: string]: ICarModel[] } = {};

  newCarForm: FormGroup = new FormGroup(
    {
      color: new FormControl('', [
        Validators.required,
        Validators.minLength(1),
      ]),
      year: new FormControl('', [
        Validators.required,
        Validators.minLength(4),
      ]),
      price: new FormControl('', [
        Validators.required,
        Validators.minLength(1),
      ]),
      info: new FormControl('', [
        Validators.required,
        Validators.minLength(10),
      ]),
      brandID: new FormControl(1, [Validators.required,]),
      modelID: new FormControl(1, [Validators.required,]),
      image: new FormControl(''),
    }
  );

  private carBrandUnsubscribes: Subscription = this.carBrandsService
    .carBrandsUpdateSubject$
    .subscribe((data) => {
      this.setBrands(data);
    });

  private carModelUnsubscribes: Subscription = this.carModelsService
    .carModelsUpdateSubject$
    .subscribe((data) => {
      this.setModels(data);
    });

  private setBrands(brands: ICarBrand[]) {
    this.carBrands = brands;
  }

  private setModels(models: ICarModel[]) {
    this.carModels = models;

    models.forEach((model) => {
      if (this.carModelsMap[model.brand._id]) {
        this.carModelsMap[model.brand._id].push(model);
      } else {
        this.carModelsMap[model.brand._id] = [model];
      }
    });
  }

  click() {
    this.carService.createCar(
      this.newCarForm.value.modelID,
      this.newCarForm.value.color,
      this.newCarForm.value.price,
      this.newCarForm.value.year,
      this.newCarForm.value.info,
      this.newCarForm.value.image

    )
      .subscribe({
          next: (res) => {
            console.log('res', res);
            this.router.navigate(['/admin/cars'])
              .then(() => {
                this.messageService.add({
                  severity: 'success',
                  summary: 'Машина добавлена',
                  detail: res.model.name
                });
              });
          },
          error: (error: HttpErrorResponse) => {
            const serverError = <ServerError>error.error;
            this.messageService.add({
              severity: 'error',
              summary: 'Ошибка создания машины',
              detail: serverError.errorText ?? 'Ошибка'
            });
          }
        }
      );
  }

  ngOnDestroy(): void {
    this.carBrandUnsubscribes.unsubscribe();
    this.carModelUnsubscribes.unsubscribe();
  }

  /**
   * Выбор файла
   * @param event
   */
  selectFile(event: any): void {
    console.log(event.target.files)
    if (event.target.files && event.target.files[0]) {
      const image = event.target.files[0];
      this.newCarForm.patchValue({
        image: image
      });
    }
  }
}
