import {Component} from '@angular/core';
import {ICarBrand} from "@models/car-brand";
import {ICar, ICarFlatBrand} from "@models/car";
import {ICarModel} from "@models/car-models";
import {CarBrandsService} from "@services/cars/car-brands.service";
import {CarService} from "@services/cars/car.service";
import {CarModelsService} from "@services/cars/car-models.service";
import {Subscription} from "rxjs";
import {ConfigService} from "@services/config/config.service";
import {Router} from "@angular/router";


@Component({
  selector: 'app-serp2',
  templateUrl: './serp2.component.html',
  styleUrls: ['./serp2.component.scss']
})
export class Serp2Component {

  brands: ICarBrand[] = [];

  cars: ICarFlatBrand[] = [];
  carsFiltered: ICarFlatBrand[] = [];

  carModelsMap: { [key: string]: ICarModel[] } = {};
  carModels: ICarModel[] = [];

  carModelsForFilter: ICarModel[] = [];

  selectedCarModels: ICarModel[] = [];

  colors: any[] = [
    {name: 'Black', class: 'bg-gray-500'},
    {name: 'Orange', class: 'bg-orange-500'},
    {name: 'Indigo', class: 'bg-indigo-500'},
    {name: 'Pink', class: 'bg-pink-500'},
  ];

  selectedBrands: any[] = [];

  selectedColors: any = [];

  constructor(
    private router: Router,
    private carBrandsService: CarBrandsService,
    private carService: CarService,
    private carModelsService: CarModelsService,
  ) {
  }


  ngOnDestroy(): void {
    this.carBrandUnsubscribes.unsubscribe();
    this.carUnsubscribes.unsubscribe();
    this.carModelUnsubscribes.unsubscribe();
  }

  ngOnInit() {
    this.carBrandsService
      .getBrands()
      .subscribe((res: ICarBrand[]) => {
        this.carBrandsService.updateCarBrandsSubs(res);
      });


    this.carService
      .getCars()
      .subscribe((res: ICar[]) => {
        this.carService.updateCarModelsSubs(res);
      });

    this.carModelsService.getModels()
      .subscribe({
        next: (data) => {
          this.carModelsService.updateCarModelsSubs(data);
        }
      });
  }

  private carBrandUnsubscribes: Subscription = this.carBrandsService
    .carBrandsUpdateSubject$
    .subscribe((data) => {
      this.setBrands(data);
    });

  private carUnsubscribes: Subscription = this.carService
    .carsUpdateSubject$
    .subscribe((data: ICar[]) => {
      this.setCars(data);
    });

  private carModelUnsubscribes: Subscription = this.carModelsService
    .carModelsUpdateSubject$
    .subscribe((data) => {
      this.setModels(data);
    });

  private setBrands(brands: ICarBrand[]) {
    this.brands = brands;
  }

  private setCars(cars: ICarFlatBrand[]) {
    cars.map((car: ICarFlatBrand) => {
      car.brand = car.model.brand;
      if (car.img) {
        car.img = ConfigService.config?.imgBaseUrl + car.img;
      }
    });
    this.cars = cars;
    this.setFilteredCars()
  }

  private setModels(models: ICarModel[]) {
    this.carModels = models;

    models.forEach((model) => {
      if (this.carModelsMap[model.brand._id]) {
        this.carModelsMap[model.brand._id].push(model);
      } else {
        this.carModelsMap[model.brand._id] = [model];
      }
    });
  }

  filterByBrand(event: any) {
    this.selectedCarModels = [];
    const brandId = event[0] ?? null;
    this.carModelsForFilter = this.carModelsMap[brandId] ?? [];
    this.setFilteredCars(brandId);
  }

  filterByModels(event: string[]) {
    console.log(event)
    // const modelIds = event.map((model: ICarModel) => model._id);
    // this.cars = this.cars.filter((car: ICarFlatBrand) => {
    //   return modelIds.includes(car.model._id);
    // });
    this.setFilteredCars(null, event);
  }

  showCar(car: ICarFlatBrand) {
    const brandName = car.brand?.name ?? 'brand';
    const modelName = car.model?.name ?? 'model';
    this.router.navigate(['/car', brandName, modelName, car.year, car._id]);
  }

  private setFilteredCars(brandID: string | null = null, modelIds: string[] = []) {
    if (brandID != null) {
      this.carsFiltered = this.cars.filter((car: ICarFlatBrand) => {
        return car.brand?._id === brandID;
      });
      return;
    }
    if (modelIds.length > 0) {
      this.carsFiltered = this.cars.filter((car: ICarFlatBrand) => {
        return modelIds.includes(car.model._id);
      });
      return;
    }
    this.carsFiltered = this.cars;
  }
}
