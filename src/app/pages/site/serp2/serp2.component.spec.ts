import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Serp2Component } from './serp2.component';

describe('Serp2Component', () => {
  let component: Serp2Component;
  let fixture: ComponentFixture<Serp2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Serp2Component ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(Serp2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
