import {LOCALE_ID, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {SiteRoutingModule} from './site-routing.module';
import {SiteComponent} from './site.component';
import {HeaderComponent} from './header/header.component';
import {FooterComponent} from './footer/footer.component';
import {ToastModule} from "primeng/toast";
import {MessageService} from "primeng/api";
import {MenubarModule} from "primeng/menubar";
import {InputTextModule} from "primeng/inputtext";
import {AboutusComponent} from './aboutus/aboutus.component';
import {TermsComponent} from './terms/terms.component';
import {RippleModule} from "primeng/ripple";
import {StyleClassModule} from "primeng/styleclass";
import {CreditComponent} from './credit/credit.component';
import {AccordionModule} from "primeng/accordion";
import {DividerModule} from "primeng/divider";
import {CheckboxModule} from "primeng/checkbox";
import {BadgeModule} from "primeng/badge";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {GalleriaModule} from "primeng/galleria";
import {ButtonModule} from "primeng/button";
import {ChipModule} from "primeng/chip";
import {MultiSelectModule} from "primeng/multiselect";
import {DropdownModule} from "primeng/dropdown";
import {SelectButtonModule} from "primeng/selectbutton";
import {DataViewModule} from "primeng/dataview";
import {RatingModule} from "primeng/rating";
import {TagModule} from "primeng/tag";
import {ImageModule} from "primeng/image";
import {DialogModule} from "primeng/dialog";
import {Serp2Component} from "@app/pages/site/serp2/serp2.component";
import {CarComponent} from "@app/pages/site/car/car.component";


@NgModule({
  declarations: [
    SiteComponent,
    HeaderComponent,
    FooterComponent,
    AboutusComponent,
    TermsComponent,
    CreditComponent,
    Serp2Component,
    CarComponent
  ],
  imports: [
    CommonModule,
    SiteRoutingModule,
    ToastModule,
    MenubarModule,
    InputTextModule,
    RippleModule,
    StyleClassModule,
    CommonModule,
    AccordionModule,
    DividerModule,
    CheckboxModule,
    BadgeModule,
    InputTextModule,
    FormsModule,
    GalleriaModule,
    RippleModule,
    ButtonModule,
    ChipModule,
    StyleClassModule,
    MultiSelectModule,
    DropdownModule,
    SelectButtonModule,
    DataViewModule,
    RatingModule,
    TagModule,
    ImageModule,
    ReactiveFormsModule,
    DialogModule
  ],
  providers: [
    {provide: LOCALE_ID, useValue: "ru-RU"},
    MessageService,
    // {provide: HTTP_INTERCEPTORS, useClass: RestInterceptor, multi: true}
  ]
})
export class SiteModule {
}
