import {Component} from '@angular/core';
import {MenuItem} from "primeng/api";
import {registerLocaleData} from "@angular/common";
import localeRu from "@angular/common/locales/ru";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {
  items: MenuItem[] = [];

  ngOnInit(): void {

    registerLocaleData(localeRu, 'ru-RU');

    this.initMenu();
  }

  private initMenu() {
    this.items = [
      {
        label: 'Модельный ряд',
        routerLink: ['/'],
        iconClass: "pi-car"
      },
      {
        label: 'Автокредитование',
        routerLink: ['/credit'],
      },
      {
        label: 'Об автосалоне',
        routerLink: ['/about-us'],
      },
    ];
  }
}
