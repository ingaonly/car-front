import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {SiteComponent} from "@app/pages/site/site.component";
import {AboutusComponent} from "@app/pages/site/aboutus/aboutus.component";
import {TermsComponent} from "@app/pages/site/terms/terms.component";
import {CreditComponent} from "@app/pages/site/credit/credit.component";
import {CarComponent} from "@app/pages/site/car/car.component";
import {Serp2Component} from "@app/pages/site/serp2/serp2.component";

const routes: Routes = [
  {
    path: '',
    component: SiteComponent,
    // title: 'SiteComponent',
    children: [

      {
        path: 'about-us',
        title: "О нас",
        component: AboutusComponent
      },
      {
        path: 'terms',
        title: "Пользовательское соглашение",
        component: TermsComponent
      },
      {
        path: 'credit',
        title: "Автокредитование",
        component: CreditComponent
      },
      {
        path: "",
        title: "Подбор автомобиля",
        component: Serp2Component
      },
      {
        path: "car/:brand/:model/:year/:id",
        component: CarComponent,
        title: "Продажа машины"
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SiteRoutingModule {
}
