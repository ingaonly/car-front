import {Component} from '@angular/core';
import {Title} from "@angular/platform-browser";

@Component({
  selector: 'app-site',
  templateUrl: './site.component.html',
  styleUrls: ['./site.component.scss']
})
export class SiteComponent {

   title?: Title;
  constructor(
    title: Title
  ) {
    this.title = title;
  }

  ngOnInit() {
  }
}
