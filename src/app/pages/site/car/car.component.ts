import {Component} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {CarService} from "@services/cars/car.service";
import {ICarFlatBrand} from "@models/car";
import {Title} from "@angular/platform-browser";
import {ConfigService} from "@services/config/config.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {OrderService} from "@services/order/order.service";
import {MessageService} from "primeng/api";
import {HttpErrorResponse} from "@angular/common/http";
import {ServerError} from "@models/errors";

@Component({
  selector: 'app-car',
  templateUrl: './car.component.html',
  styleUrls: ['./car.component.scss']
})
export class CarComponent {

  visible4: boolean = false;

  constructor(
    private router: Router,
    private carService: CarService,
    private route: ActivatedRoute,
    private title: Title,
    private orderService: OrderService,
    private messageService: MessageService,
  ) {
  }

  carID?: string;
  car?: ICarFlatBrand;
  titleText?: string;

  newOrderForm: FormGroup = new FormGroup(
    {
      name: new FormControl('', [
        Validators.required,
        Validators.minLength(1),
      ]),
      phone: new FormControl('', [
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(15),
      ]),
    }
  );

  ngOnInit(): void {

    // parse url params
    const routeIdParam = this.route.snapshot.paramMap.get('id');
    const queryIdParam = this.route.snapshot.queryParamMap.get('id');

    const paramId = routeIdParam || queryIdParam;
    if (paramId) {
      this.carID = paramId;
      this.carService
        .getCarByID(this.carID)
        .subscribe((res: ICarFlatBrand) => {
          if (res.img) {
            res.img = ConfigService.config?.imgBaseUrl + res.img;
          }
          this.car = res;
          this.setTitle(this.car);
        });
    }
  }

  private setTitle(car: ICarFlatBrand) {
    this.titleText = `${car.model?.brand?.name} ${car.model?.name} ${car.year} года за ${car.price} рублей`;
    this.title.setTitle('Продажа ' + this.titleText);
  }

  click() {
    if (!this.carID) {
      this.messageService.add({
        severity: 'error',
        summary: 'Ошибка загрузки информации о машине',
        // detail: serverError.errorText ?? 'Ошибка'
      });
      return;
    }
    // this.visible4 = true;
    console.log(this.carID)
    console.log(this.newOrderForm.value)
    this.orderService.createOrder(
      this.carID,
      this.newOrderForm.value.name,
      this.newOrderForm.value.phone
    ).subscribe({
      next: (res) => {
        console.log('res', res);
        this.visible4 = false;
        this.messageService.add({
          severity: 'success',
          summary: 'Заявка успешно создана',
          detail: 'С вами скоро свяжутся'
        });
        // this.router.navigate(['/admin/cars'])
        //   .then(() => {
        //     this.messageService.add({
        //       severity: 'success',
        //       summary: 'Машина добавлена',
        //       detail: res.model.name
        //     });
        //   });
      },
      error: (error: HttpErrorResponse) => {
        const serverError = <ServerError>error.error;
        this.messageService.add({
          severity: 'error',
          summary: 'Ошибка создания заказа',
          detail: serverError.errorText ?? 'Ошибка'
        });
      }
    })
    ;
  }
}
