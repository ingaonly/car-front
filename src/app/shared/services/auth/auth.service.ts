import {Injectable} from '@angular/core';
import {Observable} from "rxjs";
import {IAccessToken, IUser} from "@models/users";
import {UserService} from "@services/user/user.service";
import {Router} from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class AuthService {


  private storageKeyAccessToken: string = 'access_token';

  constructor(
    private userService: UserService,
    private router: Router
  ) {
  }

  getCurrentUser():Observable<IUser>{
    return this.userService.me();
  }

  /**
   * Авторизация пользователя
   * @param email
   * @param password
   */
  authUser(email: string, password: string): Observable<IAccessToken> {
    return this.userService.authUser(email, password);
  }

  registerUser(email: string, password: string, name: string, phone: string): Observable<any> {
    return this.userService.registerUser(email, password, name, phone);
  }

  /**
   * Устоновка сессии, сохранение токена
   * @param authResult
   */
  public setSession(authResult: IAccessToken) {
    localStorage.setItem(this.storageKeyAccessToken, authResult.access_token);
  }

  /**
   * Получение токена
   */
  public getToken(): string | null {
    return localStorage.getItem(this.storageKeyAccessToken);
  }

  /**
   * Удаление сессии для разлогинивания
   */
  removeToken() {
    localStorage.removeItem(this.storageKeyAccessToken);
  }

  /**
   * Если пользователь не авторизован, то перенаправляем его на страницу логина
   */
  removeTokenAndRouteToLoginPage() {
    this.removeToken();
    this.router.navigate(['admin/login']);
  }
}
