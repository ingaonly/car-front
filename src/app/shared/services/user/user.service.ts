import {Injectable} from '@angular/core';
import {Observable} from "rxjs";
import {IAccessToken, IFullIUser, IUser} from "@models/users";
import {HttpClient} from "@angular/common/http";
import {ApiService} from "@services/api/api.service";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private http: HttpClient,
    private apiService: ApiService
  ) {
  }

  /**
   * Авторизация пользователя
   * @param email
   * @param password
   */
  authUser(email: string, password: string): Observable<IAccessToken> {
    return this.http
      .post<IAccessToken>(this.apiService.getFullApiUrl(`users/login`), {email, password});
  }

  /**
   * Регистрация пользователя
   * @param email
   * @param password
   * @param name
   * @param phone
   */
  registerUser(email: string, password: string, name: string, phone: string): Observable<any> {
    const user: IFullIUser = {
      email: email,
      name: name,
      password: password,
      phone: phone
    }
    return this.http
      .post<IAccessToken>(this.apiService.getFullApiUrl(`users`), user);
  }

  me(): Observable<IUser> {
    return this.http
      .get<IUser>(this.apiService.getFullApiUrl(`users/me`));
  }

}
