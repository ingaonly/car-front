import {Injectable} from '@angular/core';
import {ApiService} from "@services/api/api.service";
import {HttpClient} from "@angular/common/http";
import {Observable, Subject} from "rxjs";
import {ICar} from "@models/car";

@Injectable({
  providedIn: 'root'
})
export class CarService {

  constructor(
    private apiService: ApiService,
    private http: HttpClient
  ) {
  }

  private carUpdateSubject = new Subject<ICar[]>();
  readonly carsUpdateSubject$ = this.carUpdateSubject.asObservable();

  updateCarModelsSubs(data: ICar[]) {
    this.carUpdateSubject.next(data);
  }

  getCars(): Observable<ICar[]> {
    return this.http
      .get<ICar[]>(this.apiService.getFullApiUrl(`cars`));
  }

  getCarByID(carID : string): Observable<ICar> {
    return this.http
      .get<ICar>(this.apiService.getFullApiUrl(`cars/` + carID));
  }

  createCar(modelID: string, color: string, price: string, year: string, info:string, image: File): Observable<ICar> {
    let formParams = new FormData();
    formParams.append("model_id", modelID);
    formParams.append("color", color);
    formParams.append("price", price);
    formParams.append("year", year);
    formParams.append("info", info);
    formParams.append("file", image);
    return this.http
      .post<ICar>(this.apiService.getFullApiUrl(`cars`), formParams);
  }
}
