import {Injectable} from '@angular/core';
import {ApiService} from "@services/api/api.service";
import {HttpClient} from "@angular/common/http";
import {Observable, Subject} from "rxjs";
import {ICarBrand} from "@models/car-brand";

@Injectable({
  providedIn: 'root'
})
export class CarBrandsService {

  constructor(
    private apiService: ApiService,
    private http: HttpClient
  ) {
  }

  private carBrandsUpdateSubject = new Subject<ICarBrand[]>();
  readonly carBrandsUpdateSubject$ = this.carBrandsUpdateSubject.asObservable();

  updateCarBrandsSubs(data: ICarBrand[]) {
    this.carBrandsUpdateSubject.next(data);
  }

  getBrands(): Observable<ICarBrand[]> {
    return this.http
      .get<ICarBrand[]>(this.apiService.getFullApiUrl(`car-brands`));
  }

  getBrandByID(id: string): Observable<ICarBrand> {
    return this.http
      .get<ICarBrand>(this.apiService.getFullApiUrl(`car-brands/${id}`));
  }

  createBrand(name: string): Observable<ICarBrand> {
    return this.http
      .post<ICarBrand>(this.apiService.getFullApiUrl(`car-brands`), {name: name});
  }

  updateBrandByID(id: string, name: string): Observable<ICarBrand> {
    return this.http
      .put<ICarBrand>(this.apiService.getFullApiUrl(`car-brands/${id}`), {name: name});
  }
}
