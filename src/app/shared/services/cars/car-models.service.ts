import {Injectable} from '@angular/core';
import {ApiService} from "@services/api/api.service";
import {HttpClient} from "@angular/common/http";
import {Observable, Subject} from "rxjs";
import {ICarModel} from "@models/car-models";

@Injectable({
  providedIn: 'root'
})
export class CarModelsService {

  constructor(
    private apiService: ApiService,
    private http: HttpClient
  ) {
  }

  private carModelsUpdateSubject = new Subject<ICarModel[]>();
  readonly carModelsUpdateSubject$ = this.carModelsUpdateSubject.asObservable();

  updateCarModelsSubs(data: ICarModel[]) {
    this.carModelsUpdateSubject.next(data);
  }

  getModels(): Observable<ICarModel[]> {
    return this.http
      .get<ICarModel[]>(this.apiService.getFullApiUrl(`car-models`));
  }

  createModel(brandId: string, name: string): Observable<ICarModel> {
    return this.http
      .post<ICarModel>(this.apiService.getFullApiUrl(`car-models`),
        {
          brand_id: brandId,
          name: name
        });
  }
}
