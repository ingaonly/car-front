import {Injectable} from '@angular/core';
import {ConfigService} from "@services/config/config.service";

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor() {
  }

  public getFullApiUrl(method: string): string {
    const apiUrl = this.getApiUrl()
    // return new URL(method, apiUrl).toString();
    return `${apiUrl}/${method}`;
  }


  private getApiUrl(): string {
    const apiUrl = ConfigService.config?.apiUrl;
    if (!apiUrl) {
      console.error('Api url is not defined');
      throw new Error('Api url is not defined');
    }
    return apiUrl
  }
}
