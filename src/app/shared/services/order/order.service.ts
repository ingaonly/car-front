import {Injectable} from '@angular/core';
import {ApiService} from "@services/api/api.service";
import {HttpClient} from "@angular/common/http";
import {Observable, Subject} from "rxjs";
import {IOrder} from "@models/order";

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  constructor(
    private apiService: ApiService,
    private http: HttpClient
  ) {
  }

  private ordersUpdateSubject = new Subject<IOrder[]>();
  readonly ordersUpdateSubject$ = this.ordersUpdateSubject.asObservable();

  updateOrdersSubs(data: IOrder[]) {
    this.ordersUpdateSubject.next(data);
  }

  createOrder(carID: string, name: string, phone: string): Observable<IOrder> {
    return this.http
      .post<IOrder>(
        this.apiService.getFullApiUrl(`orders`),
        {
          car_id: carID,
          name: name,
          phone: phone
        }
      );
  }

  getOrders(): Observable<IOrder[]> {
    return this.http
      .get<IOrder[]>(this.apiService.getFullApiUrl(`orders`));
  }
}
