import {Injectable} from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor, HttpErrorResponse
} from '@angular/common/http';
import {catchError, Observable, throwError} from 'rxjs';
import {AuthService} from "@services/auth/auth.service";

@Injectable()
export class RestInterceptor implements HttpInterceptor {
  constructor(
    private authService: AuthService
  ) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const access_token = this.authService.getToken();
    if (access_token) {
      const cloned = request.clone({
        headers: request.headers.set(
          "Authorization", "Bearer " + access_token
        )
      })
      return next.handle(cloned)
        .pipe(
          catchError((err: HttpErrorResponse) => {
              if (err.status === 401) {
                console.log('auth error')
                this.authService.removeTokenAndRouteToLoginPage();
              }
              return throwError(err);
            }
          ));
    }
    return next.handle(request)
      .pipe(
        catchError((err: HttpErrorResponse) => {
            if (err.status === 401) {
              console.log('auth error')
              this.authService.removeTokenAndRouteToLoginPage();
            }
            return throwError(err);
          }
        ));
  }
}
