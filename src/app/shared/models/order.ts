import {ICar} from "@models/car";
import {IUser} from "@models/users";

export interface IOrder {
  car: ICar;
  phone: string;
  name: string;
  date?: Date;
}

export interface IOrderFlat extends IOrder {
  manager?: IUser;
}
