import {ICarModel} from "@models/car-models";
import {IUser} from "@models/users";
import {ICarBrand} from "@models/car-brand";

export interface ICar {
  model: ICarModel;
  year: number;
  color: string;
  price: string;
  manager?: IUser;
  info?: string,
  img: string;
}

export interface ICarFlatBrand extends ICar {
  brand?: ICarBrand;
  _id?: string;
}
