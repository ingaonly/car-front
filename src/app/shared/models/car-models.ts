import {ICarBrand} from "@models/car-brand";

export interface ICarModel {
  name: string;
  brand: ICarBrand;
  _id: string;
}
