export interface IUser {
  email: string,
  phone: string,
  name: string,
  _id?: string,
}

export interface IFullIUser extends IUser{
  password: string,
}

export interface IAccessToken {
  access_token: string,
  id: string,
}
